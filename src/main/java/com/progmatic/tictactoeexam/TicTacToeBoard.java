/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class TicTacToeBoard implements Board {

    protected static Cell[][] board = new Cell[3][3];

    public TicTacToeBoard() {

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Cell c = new Cell(i, j);
                board[i][j]=c;

            }

        }

    }

    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {
        PlayerType player;
        try {
            player = board[rowIdx][colIdx].getCellsPlayer();
        } catch (ArrayIndexOutOfBoundsException e) {
            String comment = "Hibas lepes.";
            CellException ce = new com.progmatic.tictactoeexam.exceptions.CellException(rowIdx, colIdx, comment);
            throw ce;
        }
        return player;

    }

    @Override
    public void put(Cell cell) throws CellException {
        int row = cell.getRow();
        int col = cell.getCol();
        if (row > 2 || col > 2) {
            String comment = "nem jo lepes";
            CellException ce = new com.progmatic.tictactoeexam.exceptions.CellException(row, col, comment);
            throw ce;

        }
        if (board[row][col].getCellsPlayer().equals(PlayerType.EMPTY)) {
            board[row][col].setCellsPlayer(cell.getCellsPlayer());
        } else {
            String comment = "Foglalt mezo";
            CellException ce = new com.progmatic.tictactoeexam.exceptions.CellException(row, col, comment);
            throw ce;
        }

    }

    @Override
    public boolean hasWon(PlayerType p) {
        boolean won = false;

        if (board[0][0].getCellsPlayer().equals(p) && board[0][1].getCellsPlayer().equals(p) && board[0][2].getCellsPlayer().equals(p)) {
            won = true;
        } else if (board[0][0].getCellsPlayer().equals(p) && board[1][0].getCellsPlayer().equals(p) && board[2][0].getCellsPlayer().equals(p)) {
            won = true;
        } else if (board[0][0].getCellsPlayer().equals(p) && board[1][1].getCellsPlayer().equals(p) && board[2][2].getCellsPlayer().equals(p)) {
            won = true;
        } else if (board[1][0].getCellsPlayer().equals(p) && board[1][1].getCellsPlayer().equals(p) && board[1][2].getCellsPlayer().equals(p)) {
            won = true;
        } else if (board[2][0].getCellsPlayer().equals(p) && board[2][1].getCellsPlayer().equals(p) && board[2][2].getCellsPlayer().equals(p)) {
            won = true;
        } else if (board[0][1].getCellsPlayer().equals(p) && board[1][1].getCellsPlayer().equals(p) && board[2][1].getCellsPlayer().equals(p)) {
            won = true;
        } else if (board[0][2].getCellsPlayer().equals(p) && board[1][2].getCellsPlayer().equals(p) && board[2][2].getCellsPlayer().equals(p)) {
            won = true;
        } else if (board[0][2].getCellsPlayer().equals(p) && board[1][1].getCellsPlayer().equals(p) && board[2][0].getCellsPlayer().equals(p)) {
            won = true;
        }

        return won;

    }

    @Override
    public List<Cell> emptyCells() {
        List<Cell> cells = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                Cell c = board[i][j];
                if (board[i][j].getCellsPlayer().equals(PlayerType.EMPTY)) {
                    cells.add(c);
                }

            }

        }
        return cells;
    }

}
